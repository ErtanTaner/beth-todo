import htmx from "htmx.org";

declare global {
  interface Window {
    htmx: any;
  }
}

window.htmx = htmx;
