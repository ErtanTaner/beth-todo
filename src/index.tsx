import { Elysia, redirect, t } from "elysia";
import { html } from "@elysiajs/html";
import { staticPlugin } from "@elysiajs/static";
import { client } from "./db";
import TodoTable, { convertTodoToObject } from "@comps/TodoTable";
import { randomUUID } from "crypto";

const PORT = Bun.env.PORT || 300;

const BaseHtml = ({ children }: { children: JSX.Element }) => `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Todo</title>
  <script src="https://kit.fontawesome.com/91e746a27a.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="../public/index.css" />
  <script src="../public/htmx.min.js"></script>
</head>
 ${children} 
</html>
`;

const elysia = new Elysia()
  .use(html())
  .use(staticPlugin())
  .get("/", async ({ html }) => {
    const todos = (await client.execute("SELECT * FROM todos")).rows.map(
      (row) => convertTodoToObject(row)
    );
    return html(
      <BaseHtml>
        <body>
          <h1 safe>{new Date().toLocaleString()}</h1>
          <h2>Todos of today are</h2>
          <TodoTable todos={todos} />
          <button
            type="button"
            hx-get="/add"
            hx-target="#modal-container-off"
            hx-swap="outerHTML"
          >
            Add
          </button>
          <div id="modal-container-off" class="off"></div>
        </body>
      </BaseHtml>
    );
  })
  .get("/add", ({ html }) => {
    return html(
      <div id="modal-container" class="modal">
        <div class="modal-content">
          <i
            class="fa-solid fa-xmark close-modal"
            hx-get="/modal-reset"
            hx-target="#modal-container"
            hx-swap="outerHTML"
          ></i>
          <form hx-post="/add" class="add-form" hx-target="body">
            <label for="t-title">Title</label>
            <input type="text" name="title" id="t-title" />
            <label for="t-description">Description</label>
            <input type="text" name="description" id="t-description" />
            <button type="submit">Create</button>
          </form>
        </div>
      </div>
    );
  })
  .get("/modal-reset", () => <div id="modal-container-off"></div>)
  .post(
    "/add",
    async ({ body, redirect }) => {
      if (body.description && body.title)
        console.log(
          (
            await client.execute({
              sql: "INSERT INTO todos VALUES (:id, :title, :description, 0)",
              args: {
                id: randomUUID(),
                title: body.title,
                description: body.description
              }
            })
          ).toJSON()
        );
      else {
        return <div>There is a problem</div>;
      }
      return redirect("/", 303);
    },
    { body: t.Object({ title: t.String(), description: t.String() }) }
  )
  .delete(
    "/delete/:id",
    async ({ params, redirect }) => {
      console.log(params);
      await client.execute({
        sql: "DELETE FROM todos WHERE id = :id",
        args: { id: params.id }
      });

      return redirect("/", 303);
    },
    { params: t.Object({ id: t.String() }) }
  )
  .get(
    "/todo/:id",
    async ({ params, html }) => {
      const res = await client.execute({
        sql: "SELECT * FROM todos WHERE id = :id",
        args: { id: params.id }
      });
      const todo = convertTodoToObject(res.rows[0]);
      return html(
        <div id="modal-container" class="modal">
          <div class="modal-content">
            <i
              class="fa-solid fa-xmark close-modal"
              hx-get="/modal-reset"
              hx-target="#modal-container"
              hx-swap="outerHTML"
            ></i>
            {Object.keys(todo).map((key) => (
              <>
                <h2 safe>{key}</h2>
                <p safe>{todo[key as keyof typeof todo]}</p>
              </>
            ))}
          </div>
        </div>
      );
    },
    {
      params: t.Object({ id: t.String() })
    }
  )
  .get(
    "/edit/:id",
    async ({ params, html }) => {
      const res = await client.execute({
        sql: "SELECT * FROM todos WHERE id = :id",
        args: { id: params.id }
      });
      const todo = convertTodoToObject(res.rows[0]);

      return html(
        <>
          <div id="modal-container" class="modal">
            <div class="modal-content">
              <i
                class="fa-solid fa-xmark close-modal"
                hx-get="/modal-reset"
                hx-target="#modal-container"
                hx-swap="outerHTML"
              ></i>
              <form
                hx-put={`/edit/${todo.id}`}
                class="edit-form"
                hx-target="body"
              >
                <label for="t-title">Title</label>
                <input
                  type="text"
                  name="title"
                  id="t-title"
                  value={todo.title}
                />
                <label for="t-description">Description</label>
                <input
                  type="text"
                  name="description"
                  id="t-description"
                  value={todo.description}
                />
                <label for="t-is_done">Done</label>
                <select name="is_done" id="t-is_done">
                  {[0, 1].map((num) => {
                    return (
                      <option
                        value={`${num}`}
                        selected={res.rows[0]["is_done"] === num}
                      >
                        {num === 0 ? "undone" : "done"}
                      </option>
                    );
                  })}
                </select>
                <button type="submit">Update</button>
              </form>
            </div>
          </div>
        </>
      );
    },
    { params: t.Object({ id: t.String() }) }
  )
  .put(
    "/edit/:id",
    async ({ params, body, redirect }) => {
      const res = await client.execute({
        sql: "UPDATE todos SET title = :title, description = :description, is_done = :is_done WHERE id = :id",
        args: {
          title: body.title,
          description: body.description,
          is_done: body.is_done,
          id: params.id
        }
      });
      return redirect("/", 303);
    },
    {
      params: t.Object({ id: t.String() }),
      body: t.Object({
        title: t.String(),
        description: t.String(),
        is_done: t.String()
      })
    }
  );

elysia.listen(PORT, () => console.log(`TODO app is listening on ${PORT}`));
