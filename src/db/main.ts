import { createClient } from "@libsql/client";
import { randomUUID } from "crypto";
export const client = createClient({
  url: ":memory:"
});

await client.execute(
  `CREATE TABLE IF NOT EXISTS todos (
id TEXT PRIMARY KEY NOT NULL,
title TEXT UNIQUE NOT NULL,
description TEXT NOT NULL,
is_done INTEGER DEFAULT 0
);`
);

await client.execute({
  sql: "INSERT INTO todos VALUES (:id, 'complete todo app', 'You have to complete the todo app to more familiary with BETH stack', 0)",
  args: { id: randomUUID() }
});
