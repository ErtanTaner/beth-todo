import type { Row } from "@libsql/client";

export type Todo = {
  id: string;
  title: string;
  description: string;
  is_done: "done" | "undone";
};

export const convertTodoToObject = (todo: Row): Todo => {
  return {
    id: todo["id"]!.toString(),
    title: todo["title"]!.toString(),
    description: todo["description"]!.toString(),
    is_done: todo["is_done"] === 0 ? "undone" : "done"
  };
};

const TodoTable = ({ todos }: { todos: Todo[] }) => {
  return (
    <table>
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Finished</th>
        <th>Actions</th>
      </tr>
      <tbody>
        {todos.map((todo) => (
          <tr>
            <th safe>{todo.id}</th>
            <th safe>{todo.title}</th>
            <th safe>{todo.description}</th>
            <th>{todo.is_done}</th>
            <th class="actions">
              <i
                class="fa-solid fa-trash"
                hx-delete={`http://localhost:3000/delete/${todo.id}`}
                hx-confirm="Are you really sure?"
                hx-target="body"
              />
              <i
                class="fa-solid fa-magnifying-glass"
                hx-get={`/todo/${todo.id}`}
                hx-target="#modal-container-off"
                hx-swap0="outerHTML"
              />
              <i
                class="fa-solid fa-pen"
                hx-get={`/edit/${todo.id}`}
                hx-target="#modal-container-off"
              />
            </th>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default TodoTable;
